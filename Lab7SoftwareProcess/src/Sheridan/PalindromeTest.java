package Sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindrome() {
		assertFalse("hanna",false);
		assertTrue("naman",true);
		assertFalse("",false);
		assertFalse(null,false);
	}

}
